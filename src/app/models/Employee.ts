export class Employee {
  id: number;
  fio: string;
  phone: string;


  constructor(id: number, fio: string, phone: string) {
    this.id = id;
    this.fio = fio;
    this.phone = phone;
  }
}
