export class Client {
  id: number;
  fio: string;
  phone: string;
  address: string;
  password?: string;


  constructor(id: number, fio: string, phone: string, address: string, password = "") {
    this.id = id;
    this.fio = fio;
    this.phone = phone;
    this.address = address;
    this.password = password;
  }
}
