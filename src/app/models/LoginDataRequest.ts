import {Client} from "./Client";

export interface LoginDataResponse {
  role: string;
  typeToken: string;
  token: string;
  client?: Client;
}
