import {Employee} from "./Employee";
import {TypeService} from "./TypeService";
import {Service} from "./Service";
import {Client} from "./Client";

export class Appointment {
  id: number;
  service: Service;
  client: Client;
  comment: string

  constructor(id: number, service: Service, client: Client, comment: string) {
    this.id = id;
    this.service = service;
    this.client = client;
    this.comment = comment;
  }
}
