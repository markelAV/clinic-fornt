import {Employee} from "./Employee";
import {TypeService} from "./TypeService";

export class Service {
  id: number;
  name: string;
  description: string;

  price: string;
  employee: Employee;
  typeService: TypeService;


  constructor(id: number, name: string, description: string, price: string, employee: Employee, typeService: TypeService) {
    this.id = id;
    this.name = name;
    this.description = description;
    this.price = price;
    this.employee = employee;
    this.typeService = typeService;
  }
}
