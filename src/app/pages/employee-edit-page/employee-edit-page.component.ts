import { Component, OnInit } from '@angular/core';
import {Employee} from "../../models/Employee";
import {Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {HttpService} from "../../services/http.service";

@Component({
  selector: 'app-employee-edit-page',
  templateUrl: './employee-edit-page.component.html',
  styleUrls: ['./employee-edit-page.component.css']
})
export class EmployeeEditPageComponent implements OnInit {

  public employee!: Employee;
  private employeeId: any;
  private routeSubscription: Subscription;
  constructor(private route: ActivatedRoute, private httpService: HttpService) {
    this.routeSubscription = route.params.subscribe(params => this.employeeId = params['id']);

    if (this.employeeId != null && this.employeeId.length > -1) {
      httpService.getEmployeeById(this.employeeId).subscribe(
        (data) => {
          this.employee = data;
        },
        error => {
          alert(error);
          this.employee = new Employee(-1, "", "");
        }
      )
    } else {
      this.employee = new Employee(-1, "", "");
    }
  }

  ngOnInit(): void {
  }

  save() {
    if(this.employeeId != null && this.employeeId > -1 && this.employee.id > -1) {
      this.httpService.updateEmployee(this.employee).subscribe(
        (data) => {
          alert("Type Employee updated" );
          window.location.href='/employees';
        }, error => {
          console.log(error);
          alert("Error update");
        }
      );
    } else {
      this.httpService.createEmployee(this.employee).subscribe(
        (data) => {
          alert("Employee success created with id: " + data.id);
          window.location.href='/employees';
        }, error => {
          console.log(error);
          alert("Error created");
        }
      );
    }
  }

  delete() {
    if(this.employeeId != null && this.employeeId > -1 && this.employee.id > -1) {
      this.httpService.deleteEmployee(this.employee).subscribe(
        (data) => {
          alert('Success deleted')
          window.location.href='/employees';
        },
        error => {
          console.log(error);
          alert('Error. Not success deleted');
        }
      )
    }
  }
}
