import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MainClientPageComponent } from './main-client-page.component';

describe('MainClientPageComponent', () => {
  let component: MainClientPageComponent;
  let fixture: ComponentFixture<MainClientPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MainClientPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MainClientPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
