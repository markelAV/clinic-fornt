import { Component, OnInit } from '@angular/core';
import {Service} from "../../models/Service";
import {Employee} from "../../models/Employee";
import {TypeService} from "../../models/TypeService";
import {Subscription} from "rxjs";
import {Client} from "../../models/Client";
import {Appointment} from "../../models/Appointment";
import {ActivatedRoute} from "@angular/router";
import {HttpService} from "../../services/http.service";

@Component({
  selector: 'app-appointment-edit-page',
  templateUrl: './appointment-edit-page.component.html',
  styleUrls: ['./appointment-edit-page.component.css']
})
export class AppointmentEditPageComponent implements OnInit {

  appointment!: Appointment;
  services!: Service[];
  clients!: Client[];

  private appointmentId: any;
  private routeSubscription: Subscription;

  constructor(private route: ActivatedRoute, private httpService: HttpService) {
    this.routeSubscription = route.params.subscribe(params => this.appointmentId = params['id']);
    this.httpService.getAllServices().subscribe(
      (data) => {
        this.services = data;
        console.log(this.services);
      }, error => alert('Error')
    );
    this.httpService.getAllClients().subscribe(
      (data) => {
        this.clients = data;
        console.log(this.clients);
      }, error => alert('Error')
    );

    if (this.appointmentId != null && this.appointmentId > 0) {
      this.httpService.getAppointmentById(this.appointmentId).subscribe(
        (data) => this.appointment = data,
        error => alert('Error')
      );
    } else {
      let service = new Service(-1, "", "", "", new Employee(-1, "", ""), new TypeService(-1, "", ""));
      let client = new Client(-1, "", "", "", "")
      this.appointment = new Appointment(-1, service, client, "");
    }
  }

  ngOnInit(): void {
  }

  save() {
    console.log(this.appointment);
    if(this.appointment.id > 0) {
      this.httpService.updateAppointment(this.appointment).subscribe(
        (data) => {
          alert('Appointment success updated');
          window.location.href = '/appointments';
        }, error => alert("Error update")
      )
    } else {
      this.httpService.createAppointment(this.appointment).subscribe(
        (data) => {
          alert('Appointment success created with id: ' + data.id);
          window.location.href = '/appointments';
        }, error => alert("Error create")
      );
    }
  }

  delete() {
    console.log(this.appointment);
    this.httpService.deleteAppointment(this.appointment).subscribe(
      data => {
        alert('Appointment success deleted');
        window.location.href = '/appointments';
      },
      error => {
        alert('error')
      }
    );
  }
}
