import { Component, OnInit } from '@angular/core';
import {Employee} from "../../models/Employee";
import {AuthorizeService} from "../../services/authorize.service";
import {HttpService} from "../../services/http.service";

@Component({
  selector: 'app-employees-page',
  templateUrl: './employees-page.component.html',
  styleUrls: ['./employees-page.component.css']
})
export class EmployeesPageComponent implements OnInit {

  employees!: Employee[];

  constructor(private authService: AuthorizeService, httpService: HttpService) {
    httpService.getAllEmployees().subscribe(
      (data) => {
        this.employees = data;
      }, error => {
        console.log(error);
        alert("Error");
      }
    )
  }

  ngOnInit(): void {
  }

  public isAdmin(): boolean {
    return this.authService.isAuthorize() && this.authService.isAdmin();
  }

  editEmployee(id: number) {
    window.location.href = '/employee/' + id;

  }
}
