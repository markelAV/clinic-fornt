import {Component, OnInit} from '@angular/core';
import {Appointment} from "../../models/Appointment";
import {HttpService} from "../../services/http.service";
import {AuthorizeService} from "../../services/authorize.service";

@Component({
  selector: 'app-appointments-page',
  templateUrl: './appointments-page.component.html',
  styleUrls: ['./appointments-page.component.css']
})
export class AppointmentsPageComponent implements OnInit {

  appointments!: Appointment[];

  constructor(private httpService: HttpService, private auth: AuthorizeService) {
    this.httpService.getAllAppointments().subscribe(
      (data) => {
        this.appointments = data;
      },
      error => {alert("Error")}
    );
  }

  ngOnInit(): void {
  }

  public isAdmin(): boolean {
    return this.auth.isAuthorize() && this.auth.isAdmin();
  }

  editAppointment(id: number) {
    window.location.href = 'appointment/' + id;
  }
}
