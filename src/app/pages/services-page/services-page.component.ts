import { Component, OnInit } from '@angular/core';
import {HttpService} from "../../services/http.service";
import {AuthorizeService} from "../../services/authorize.service";
import {Service} from "../../models/Service";

@Component({
  selector: 'app-services-page',
  templateUrl: './services-page.component.html',
  styleUrls: ['./services-page.component.css']
})
export class ServicesPageComponent implements OnInit {

  services!: Service[];


  constructor(private httpService: HttpService, private auth: AuthorizeService) {
    httpService.getAllServices().subscribe(
      (data) => {
        this.services = data;
      }, error => {
        console.log(error);
        alert("Error");
      }
    )
  }

  ngOnInit(): void {
  }

  public isAdmin(): boolean {
    return this.auth.isAuthorize() && this.auth.isAdmin();
  }

  editService(id: number) {
    window.location.href = "/service/" + id;
  }
}
