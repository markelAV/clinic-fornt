import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CatalogServicesPageComponent } from './catalog-services-page.component';

describe('CatalogServicesPageComponent', () => {
  let component: CatalogServicesPageComponent;
  let fixture: ComponentFixture<CatalogServicesPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CatalogServicesPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CatalogServicesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
