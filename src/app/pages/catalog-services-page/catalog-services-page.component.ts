import {Component, OnInit} from '@angular/core';
import {Service} from "../../models/Service";
import {HttpService} from "../../services/http.service";
import {AuthorizeService} from "../../services/authorize.service";
import {Appointment} from "../../models/Appointment";

@Component({
  selector: 'app-catalog-services-page',
  templateUrl: './catalog-services-page.component.html',
  styleUrls: ['./catalog-services-page.component.css']
})
export class CatalogServicesPageComponent implements OnInit {

  services!: Service[];

  constructor(private httpService: HttpService, private auth: AuthorizeService) {
    httpService.getAllServices().subscribe(
      (data) => {
        this.services = data;
      }, error => {
        console.log(error);
        alert("Error");
      }
    )
  }

  ngOnInit(): void {
  }

  createAppointment(id: number) {
    let client = this.auth.getClient();
    let service = this.getServiceById(id);
    if (service != null && client != null) {
      let appointment = new Appointment(-1, service, client, "");
      this.httpService.createAppointment(appointment).subscribe(
        (data) => {
          alert("Вы успешно щаписанны на услугу " + data.service.name + "Id записи " + data.id);
        },
        error => {
          alert("Упс.. Произошла ошибка!")
        }
      )
    }
  }

  isUser(): boolean {
    return this.auth.isAuthorize() && this.auth.isUser();
  }

  private getServiceById(id: number): Service | null {
    for (let service of this.services) {
      if (service.id == id) {
        return service;
      }
    }
    return null;
  }
}
