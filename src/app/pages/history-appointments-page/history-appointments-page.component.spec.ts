import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoryAppointmentsPageComponent } from './history-appointments-page.component';

describe('HistoryAppointmentsPageComponent', () => {
  let component: HistoryAppointmentsPageComponent;
  let fixture: ComponentFixture<HistoryAppointmentsPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HistoryAppointmentsPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoryAppointmentsPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
