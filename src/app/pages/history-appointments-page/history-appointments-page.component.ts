import { Component, OnInit } from '@angular/core';
import {Appointment} from "../../models/Appointment";
import {HttpService} from "../../services/http.service";
import {AuthorizeService} from "../../services/authorize.service";

@Component({
  selector: 'app-history-appointments-page',
  templateUrl: './history-appointments-page.component.html',
  styleUrls: ['./history-appointments-page.component.css']
})
export class HistoryAppointmentsPageComponent implements OnInit {

  appointments: Appointment[] = [];

  constructor(private httpService: HttpService, private auth: AuthorizeService) {
    let client = auth.getClient();
    if (client != null) {
      this.httpService.getAppointmentsClient(client.id).subscribe(
        (data) => {
          this.appointments = data;
        },
        error => {alert("Error")}
      );
    }

  }

  ngOnInit(): void {
  }

}
