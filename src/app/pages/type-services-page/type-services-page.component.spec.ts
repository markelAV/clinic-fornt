import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeServicesPageComponent } from './type-services-page.component';

describe('TypeServicesPageComponent', () => {
  let component: TypeServicesPageComponent;
  let fixture: ComponentFixture<TypeServicesPageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TypeServicesPageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeServicesPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
