import { Component, OnInit } from '@angular/core';
import {HttpService} from "../../services/http.service";
import {TypeService} from "../../models/TypeService";
import {AuthorizeService} from "../../services/authorize.service";

@Component({
  selector: 'app-type-services-page',
  templateUrl: './type-services-page.component.html',
  styleUrls: ['./type-services-page.component.css']
})
export class TypeServicesPageComponent implements OnInit {

  typeServices!: TypeService[];
  constructor(private httpService: HttpService, private authService: AuthorizeService) {
    httpService.getAllTypeServices().subscribe(
      (data) => {
        this.typeServices = data;
      }, error => {console.log(error)}
    )
  }

  public isAdmin(): boolean {
    return this.authService.isAuthorize() && this.authService.isAdmin();
  }

  ngOnInit(): void {
  }

  public editTypeService(id: number) {
    window.location.href='type-service/' + id;
  }

}
