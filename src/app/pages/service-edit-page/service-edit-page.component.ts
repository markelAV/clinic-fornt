import {Component, OnInit} from '@angular/core';
import {Service} from "../../models/Service";
import {Employee} from "../../models/Employee";
import {TypeService} from "../../models/TypeService";
import {Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {HttpService} from "../../services/http.service";

@Component({
  selector: 'app-service-edit-page',
  templateUrl: './service-edit-page.component.html',
  styleUrls: ['./service-edit-page.component.css']
})
export class ServiceEditPageComponent implements OnInit {

  service!: Service;
  employees!: Employee[];
  typeServices!: TypeService[];
  private serviceId: any;
  private routeSubscription: Subscription;

  constructor(private route: ActivatedRoute, private httpService: HttpService) {
    this.routeSubscription = route.params.subscribe(params => this.serviceId = params['id']);
    this.httpService.getAllTypeServices().subscribe(
      (data) => {
        this.typeServices = data;
        console.log(this.typeServices);
      }, error => alert('Error')
    );
    this.httpService.getAllEmployees().subscribe(
      (data) => {
        this.employees = data;
        console.log(this.employees);
      }, error => alert('Error')
    );

    if (this.serviceId != null && this.serviceId > 0) {
      this.httpService.getServiceById(this.serviceId).subscribe(
        (data) => this.service = data,
        error => alert('Error')
      );
    } else {
      this.service = new Service(-1, "", "", "", new Employee(-1, "", ""), new TypeService(-1, "", ""));
    }
  }

  ngOnInit(): void {
  }

  save() {
    console.log(this.service);
    if(this.service.id > 0) {
      this.httpService.updateService(this.service).subscribe(
        (data) => {
          alert('Service success updated');
          window.location.href = '/services';
        }, error => alert("Error update")
      )
    } else {
      this.httpService.createService(this.service).subscribe(
        (data) => {
          alert('Service success created with id: ' + data.id);
          window.location.href = '/services';
        }, error => alert("Error create")
      );
    }
  }

  delete() {
    console.log(this.service);
    this.httpService.deleteService(this.service).subscribe(
      data => {
        alert('Service success deleted');
        window.location.href = '/services';
      },
      error => {
        alert('error')
      }
    );
  }
}
