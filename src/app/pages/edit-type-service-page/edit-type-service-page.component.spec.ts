import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EditTypeServicePageComponent } from './edit-type-service-page.component';

describe('EditTypeServicePageComponent', () => {
  let component: EditTypeServicePageComponent;
  let fixture: ComponentFixture<EditTypeServicePageComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EditTypeServicePageComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EditTypeServicePageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
