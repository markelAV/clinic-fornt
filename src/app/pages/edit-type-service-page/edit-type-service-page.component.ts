import { Component, OnInit } from '@angular/core';
import {TypeService} from "../../models/TypeService";
import {Subscription} from "rxjs";
import {ActivatedRoute} from "@angular/router";
import {HttpService} from "../../services/http.service";

@Component({
  selector: 'app-edit-type-service-page',
  templateUrl: './edit-type-service-page.component.html',
  styleUrls: ['./edit-type-service-page.component.css']
})
export class EditTypeServicePageComponent implements OnInit {

  public typeService!: TypeService;
  private typeServiceId: any;
  private routeSubscription: Subscription;

  constructor(private route: ActivatedRoute, private httpService: HttpService) {
    this.routeSubscription = route.params.subscribe(params => this.typeServiceId = params['id']);

    if (this.typeServiceId != null && this.typeServiceId.length > -1) {
      httpService.getTypeServiceById(this.typeServiceId).subscribe(
        (data) => {
          this.typeService = data;
        },
        error => {
          alert(error);
          this.typeService =  new TypeService(-1, "", "");
        }
      )
    } else {
      this.typeService = new TypeService(-1, "", "");
    }

  }

  ngOnInit(): void {
  }

  save() {
    if(this.typeServiceId != null && this.typeServiceId > -1 && this.typeService.id > -1) {
      this.httpService.updateTypeService(this.typeService).subscribe(
        (data) => {
          alert("Type Service success updated" );
          window.location.href='/type-services';
        }, error => {
          console.log(error);
          alert("Error update");
        }
      );
    } else {
      this.httpService.createTypeService(this.typeService).subscribe(
        (data) => {
          alert("Type Service success created with id: " + data.id);
          window.location.href='/type-services';
        }, error => {
          console.log(error);
          alert("Error created");
        }
      );
    }
  }

  delete() {
    if(this.typeServiceId != null && this.typeServiceId > -1 && this.typeService.id > -1) {
      this.httpService.deleteTypeService(this.typeService).subscribe(
        (data) => {
          alert('Success deleted')
          window.location.href='/type-services';
        },
        error => {
          console.log(error);
          alert('Error. Not success deleted');
        }
      )
    }
  }
}
