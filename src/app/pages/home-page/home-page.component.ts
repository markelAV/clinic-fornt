import { Component, OnInit } from '@angular/core';
import {AuthorizeService} from "../../services/authorize.service";

@Component({
  selector: 'app-home-page',
  templateUrl: './home-page.component.html',
  styleUrls: ['./home-page.component.css']
})
export class HomePageComponent implements OnInit {

  constructor(private authService: AuthorizeService) { }

  ngOnInit(): void {
  }
  public isAdmin(): boolean {
    return this.authService.isAuthorize() && this.authService.isAdmin();
  }
  public isClient(): boolean {
    return this.authService.isAuthorize() && this.authService.isUser();
  }

}
