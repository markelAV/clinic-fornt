import { Component, OnInit } from '@angular/core';
import {AuthorizeService} from "../../services/authorize.service";

@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit {
  username: string = "";
  password: string = "";
  constructor(private auth: AuthorizeService) { }

  ngOnInit(): void {
  }

  login() {
    console.log(this.username);
    console.log(this.password);
    if(this.username.length > 3 && this.password.length > 3) {
      this.auth.login(this.username, this.password).subscribe(
        (data: boolean) => {
          if(data) {
            window.location.href='';
          } else {
            alert('Email or password is not valid');
          }
        }
      )
    }
  }
}
