import { Component, OnInit } from '@angular/core';
import {AuthorizeService} from "../../services/authorize.service";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  constructor(private auth: AuthorizeService) { }

  ngOnInit(): void {
  }

  isAuthorize() {
    return this.auth.isAuthorize();
  }

  logout() {
    this.auth.logout();
  }

}
