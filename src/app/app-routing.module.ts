import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {HomePageComponent} from "./pages/home-page/home-page.component";
import {MainAdminPageComponent} from "./pages/main-admin-page/main-admin-page.component";
import {MainClientPageComponent} from "./pages/main-client-page/main-client-page.component";
import {TypeServicesPageComponent} from "./pages/type-services-page/type-services-page.component";
import {EditTypeServicePageComponent} from "./pages/edit-type-service-page/edit-type-service-page.component";
import {EmployeesPageComponent} from "./pages/employees-page/employees-page.component";
import {EmployeeEditPageComponent} from "./pages/employee-edit-page/employee-edit-page.component";
import {ServicesPageComponent} from "./pages/services-page/services-page.component";
import {ServiceEditPageComponent} from "./pages/service-edit-page/service-edit-page.component";
import {AppointmentsPageComponent} from "./pages/appointments-page/appointments-page.component";
import {AppointmentEditPageComponent} from "./pages/appointment-edit-page/appointment-edit-page.component";
import {HistoryAppointmentsPageComponent} from "./pages/history-appointments-page/history-appointments-page.component";
import {CatalogServicesPageComponent} from "./pages/catalog-services-page/catalog-services-page.component";
import {LoginPageComponent} from "./pages/login-page/login-page.component";

const routes: Routes = [
  {
    path: '',
    component: HomePageComponent
  },
  {
    path: 'admin/main',
    component: MainAdminPageComponent
  },
  {
    path: 'user/main',
    component: MainClientPageComponent
  },
  {
    path: 'type-services',
    component: TypeServicesPageComponent
  },
  {
    path: 'type-service/:id',
    component: EditTypeServicePageComponent
  },
  {
    path: 'type-service',
    component: EditTypeServicePageComponent
  },
  {
    path: 'employees',
    component: EmployeesPageComponent
  },
  {
    path: 'employee/:id',
    component: EmployeeEditPageComponent
  },
  {
    path: 'employee',
    component: EmployeeEditPageComponent
  },
  {
    path: 'services',
    component: ServicesPageComponent
  },
  {
    path: 'service/:id',
    component: ServiceEditPageComponent
  },
  {
    path: 'service',
    component: ServiceEditPageComponent
  },
  {
    path: 'appointments',
    component: AppointmentsPageComponent
  },
  {
    path: 'appointment/:id',
    component: AppointmentEditPageComponent
  },
  {
    path: 'appointment',
    component: AppointmentEditPageComponent
  },
  {
    path: 'history-appointment',
    component: HistoryAppointmentsPageComponent
  },
  {
    path: 'catalog-services',
    component: CatalogServicesPageComponent
  },
  {
    path: 'create-appointment',
    component: CatalogServicesPageComponent
  },
  {
    path: 'login',
    component: LoginPageComponent
  },
  {
    path: '**',
    component: HomePageComponent
  }
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule {
}
