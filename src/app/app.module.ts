import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { MainAdminPageComponent } from './pages/main-admin-page/main-admin-page.component';
import { MainClientPageComponent } from './pages/main-client-page/main-client-page.component';
import { TypeServicesPageComponent } from './pages/type-services-page/type-services-page.component';
import { EditTypeServicePageComponent } from './pages/edit-type-service-page/edit-type-service-page.component';
import { EmployeesPageComponent } from './pages/employees-page/employees-page.component';
import { EmployeeEditPageComponent } from './pages/employee-edit-page/employee-edit-page.component';
import { ServicesPageComponent } from './pages/services-page/services-page.component';
import { ServiceEditPageComponent } from './pages/service-edit-page/service-edit-page.component';
import { AppointmentsPageComponent } from './pages/appointments-page/appointments-page.component';
import { AppointmentEditPageComponent } from './pages/appointment-edit-page/appointment-edit-page.component';
import { HistoryAppointmentsPageComponent } from './pages/history-appointments-page/history-appointments-page.component';
import { CatalogServicesPageComponent } from './pages/catalog-services-page/catalog-services-page.component';
import { CreateAppointmentPageComponent } from './pages/create-appointment-page/create-appointment-page.component';
import { LoginPageComponent } from './pages/login-page/login-page.component';
import { HeaderComponent } from './components/header/header.component';
import {AppRoutingModule} from "./app-routing.module";
import {HTTP_INTERCEPTORS, HttpClientModule} from "@angular/common/http";
import {FormsModule} from "@angular/forms";
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {AuthorizeService} from "./services/authorize.service";
import {AuthInterceptorService} from "./services/auth-interceptor.service";

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent,
    MainAdminPageComponent,
    MainClientPageComponent,
    TypeServicesPageComponent,
    EditTypeServicePageComponent,
    EmployeesPageComponent,
    EmployeeEditPageComponent,
    ServicesPageComponent,
    ServiceEditPageComponent,
    AppointmentsPageComponent,
    AppointmentEditPageComponent,
    HistoryAppointmentsPageComponent,
    CatalogServicesPageComponent,
    CreateAppointmentPageComponent,
    LoginPageComponent,
    HeaderComponent
  ],
  imports: [
    AppRoutingModule,
    BrowserModule,
    FormsModule,
    HttpClientModule,
    NgbModule,
  ],
  providers: [
    { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
