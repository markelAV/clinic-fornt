import { Injectable } from '@angular/core';
import {Client} from "../models/Client";
import {Observable, Subject} from "rxjs";
import { LoginDataResponse} from "../models/LoginDataRequest";
import {HttpService} from "./http.service";
import {TokenStorageService} from "./token-storage.service";

export interface User {
  role: string;
  client?: Client;
}
export const ROLES: Map<String, String> = new Map([
  ["Admin", "ADMIN"],
  ["Manager", "MANAGER"],
  ["Buyer", "BUYER"]]);
const USER_KEY = "User";

@Injectable({
  providedIn: 'root'
})
export class AuthorizeService {
  private user?: User;

  constructor(private httpService: HttpService, private tokenStorage: TokenStorageService) { }
  public isAdmin(): boolean {
    let user = this.getUser();
    if (user != undefined) {
      return user.role == "ADMIN";
    }
    return false;
  }
  public isUser(): boolean {
    let user = this.getUser();
    if (user != undefined) {
      return user.role == "CLIENT";
    }
    return false;
  }
  public isAuthorize(): boolean {
    let user = this.getUser();
    if (user != undefined) {
      return true;
    }
    return false;
  }

  public getClient(): Client | null {
    return this.getUser()?.client || null;
  }

  public login(login: string, password: string): Observable<boolean> {
    console.log("kek");
    let event: Subject<boolean> = new Subject<boolean>();
    this.httpService.login(login, password).subscribe(
      (data: LoginDataResponse) => {
        console.log(data);
        if (data.token != null) {
          this.tokenStorage.saveToken(data.token);
          this.tokenStorage.saveTokenType(data.typeToken);
        }
        if(data.role != null) {
          let user: User = {role: data.role};
          if (data.client != null) {
            user.client = data.client;
          }
          this.saveUser(user);
        }

        event.next(true);
      },
      error => {
        event.next(false);

      }
    )
    return event;

  }
  public saveUser(user: User): void {
    this.user = user;
    localStorage.setItem(USER_KEY, JSON.stringify(this.user));
  }
  public getUser(): User | undefined {
    let itemUser = localStorage.getItem(USER_KEY);
    if (itemUser != null) {
      this.user = JSON.parse(itemUser);
    }
    return this.user;
  }

  logout() {
    this.httpService.logout().subscribe(
      next => {
        window.location.href = '/';
      },
      error => {
        console.log("Some error");
      }
    );
    this.user = undefined;
    localStorage.removeItem(USER_KEY);
    this.tokenStorage.clearTokenInfo();
  }
}
