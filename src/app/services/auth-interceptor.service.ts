import { Injectable } from '@angular/core';
import {HTTP_INTERCEPTORS, HttpErrorResponse, HttpHandler, HttpInterceptor, HttpRequest} from "@angular/common/http";
import {BehaviorSubject, catchError, switchMap, take, throwError} from "rxjs";
import {TokenStorageService} from "./token-storage.service";
import {AuthorizeService} from "./authorize.service";

const TOKEN_HEADER_KEY = 'Authorization';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptorService implements HttpInterceptor {


  constructor(private tokenService: TokenStorageService, private authService: AuthorizeService) { }

  intercept(req: HttpRequest<any>, next: HttpHandler) {
    //https://www.bezkoder.com/angular-12-refresh-token/

    let authReq = req;
    const token = this.tokenService.getToken();
    const tokenType = this.tokenService.getTokenType();
    if (token != null) {
      authReq = req.clone({ headers: req.headers.set(TOKEN_HEADER_KEY, tokenType + token)});
    }
    return next.handle(authReq).pipe(catchError(error => {
      if (error instanceof HttpErrorResponse && !authReq.url.includes('auth/login') && error.status === 401) {
        console.log("logout");
        this.authService.logout();
      }

      return throwError(error);
    }));
  }


}

export const httpInterceptorProviders = [
  { provide: HTTP_INTERCEPTORS, useClass: AuthInterceptorService, multi: true }
];
