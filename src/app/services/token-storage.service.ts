import { Injectable } from '@angular/core';

const TOKEN_KEY = 'Authorization'
const REFRESH_TOKEN_KEY = 'RefreshToken';
const TOKEN_TYPE = 'TokenType';

@Injectable({
  providedIn: 'root'
})
export class TokenStorageService {

  constructor() { }

  deleteToken() {
    window.sessionStorage.removeItem(TOKEN_KEY);
  }
  clearTokenInfo() {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.removeItem(TOKEN_TYPE);
    window.sessionStorage.removeItem(REFRESH_TOKEN_KEY);
  }

  public saveToken(token: string) {
    window.sessionStorage.removeItem(TOKEN_KEY);
    window.sessionStorage.setItem(TOKEN_KEY, token);
  }

  public saveTokenType(type: string) {
    window.sessionStorage.removeItem(TOKEN_TYPE);
    window.sessionStorage.setItem(TOKEN_TYPE, type);
  }


  public getToken(): string | null {
    return sessionStorage.getItem(TOKEN_KEY);
  }

  public getTokenType(): string | null {
    return sessionStorage.getItem(TOKEN_TYPE);
  }

}
