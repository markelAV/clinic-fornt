import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {TypeService} from "../models/TypeService";
import {Employee} from "../models/Employee";
import {Service} from "../models/Service";
import {Appointment} from "../models/Appointment";
import {Client} from "../models/Client";

const hostServer = "http://localhost";
const portServer = "8080";
const urlServer = hostServer + ':' + portServer;
const routes = new Map<string, string>([
  ['login', urlServer + '/auth/login'],
  ['logout', urlServer + '/auth/logout'],
  ['registration', urlServer + '/auth/registration'],

  //Admin

  //TypeServices
  ['getAllTypeServices', urlServer + '/main/type-services'],
  ['getTypeServiceById', urlServer + '/main/type-service?type_id='],
  ['createTypeService', urlServer + '/admin/type-service'],
  ['updateTypeService', urlServer + '/admin/type-service'],
  ['deleteTypeService', urlServer + '/admin/type-service'],

  //Employees
  ['getAllEmployees', urlServer + '/main/employees'],
  ['getEmployeeById', urlServer + '/main/employee?employee_id='],
  ['createEmployee', urlServer + '/admin/employee'],
  ['updateEmployee', urlServer + '/admin/employee'],
  ['deleteEmployee', urlServer + '/admin/employee'],

  //Services
  ['getAllServices', urlServer + '/main/services'],
  ['getServiceById', urlServer + '/main/service?service_id='],
  ['createService', urlServer + '/admin/service'],
  ['updateService', urlServer + '/admin/service'],
  ['deleteService', urlServer + '/admin/service'],

  //Appointments
  ['getAllAppointments', urlServer + '/client/appointments'],
  ['getAppointmentsByClient', urlServer + '/client/appointments?client_id='],
  ['getAppointmentById', urlServer + '/client/appointment?appointment_id='],
  ['createAppointment', urlServer + '/client/appointment'],
  ['updateAppointment', urlServer + '/admin/appointment'],
  ['deleteAppointment', urlServer + '/admin/appointment'],

  //Clients
  ['getAllClients', urlServer + '/client/clients'],
  ['getClientById', urlServer + '/client/client?client_id='],
  ['createClient', urlServer + '/admin/client'],
  ['updateClient', urlServer + '/admin/client'],
  ['deleteClient', urlServer + '/admin/client'],

]);

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  constructor(private http: HttpClient) { }

  login(username: string, password: string): Observable<any> {
    console.log(routes.get('login')!);
    return this.http.post<any>(routes.get('login')!,{login: username, password: password});
  }

  logout() {
   return this.http.post(routes.get('logout')!,{});
 }

  //TypeServices

  getAllTypeServices(): Observable<TypeService[]> {
    return this.http.get<TypeService[]>(routes.get('getAllTypeServices')!);
  }

  getTypeServiceById(id: number): Observable<TypeService> {
    return this.http.get<TypeService>(routes.get('getTypeServiceById')! + id);
  }

  createTypeService(typeService: TypeService): Observable<TypeService> {
    return this.http.post<TypeService>(routes.get('createTypeService')!, typeService);
  }

  updateTypeService(typeService: TypeService): Observable<TypeService> {
    return this.http.put<TypeService>(routes.get('updateTypeService')!, typeService);
  }
  deleteTypeService(typeService: TypeService): Observable<any> {
    return this.http.delete<any>(routes.get('deleteTypeService')!, {body: typeService});
  }

  //Employees

  getAllEmployees(): Observable<Employee[]> {
    return this.http.get<Employee[]>(routes.get('getAllEmployees')!);
  }

  getEmployeeById(id: number): Observable<Employee> {
    return this.http.get<Employee>(routes.get('getEmployeeById')! + id);
  }

  createEmployee(employee: Employee): Observable<Employee> {
    return this.http.post<Employee>(routes.get('createEmployee')!, employee);
  }

  updateEmployee(employee: Employee): Observable<Employee> {
    return this.http.put<Employee>(routes.get('updateEmployee')!, employee);
  }
  deleteEmployee(employee: Employee): Observable<any> {
    return this.http.delete<any>(routes.get('deleteEmployee')!, {body: employee});
  }

  //Services

  getAllServices(): Observable<Service[]> {
    return this.http.get<Service[]>(routes.get('getAllServices')!);
  }

  getServiceById(id: number): Observable<Service> {
    return this.http.get<Service>(routes.get('getServiceById')! + id);
  }

  createService(service: Service): Observable<Service> {
    return this.http.post<Service>(routes.get('createService')!, service);
  }

  updateService(service: Service): Observable<Service> {
    return this.http.put<Service>(routes.get('updateService')!, service);
  }
  deleteService(service: Service): Observable<any> {
    return this.http.delete<any>(routes.get('deleteService')!, {body: service});
  }

  //Appointments

  getAllAppointments(): Observable<Appointment[]> {
    return this.http.get<Appointment[]>(routes.get('getAllAppointments')!);
  }

  getAppointmentsClient(idClient: number): Observable<Appointment[]> {
    return this.http.get<Appointment[]>(routes.get('getAppointmentsByClient')! +idClient);
  }

  getAppointmentById(id: number): Observable<Appointment> {
    return this.http.get<Appointment>(routes.get('getAppointmentById')! + id);
  }

  createAppointment(appointment: Appointment): Observable<Appointment> {
    return this.http.post<Appointment>(routes.get('createAppointment')!, appointment);
  }

  updateAppointment(appointment: Appointment): Observable<Appointment> {
    return this.http.put<Appointment>(routes.get('updateAppointment')!, appointment);
  }
  deleteAppointment(appointment: Appointment): Observable<any> {
    return this.http.delete<any>(routes.get('deleteAppointment')!, {body: appointment});
  }

  //Clients

  getAllClients(): Observable<Client[]> {
    return this.http.get<Client[]>(routes.get('getAllClients')!);
  }

  getClientById(id: number): Observable<Client> {
    return this.http.get<Client>(routes.get('getClientById')! + id);
  }

  createClient(appointment: Client): Observable<Client> {
    return this.http.post<Client>(routes.get('createClient')!, appointment);
  }

  updateClient(appointment: Client): Observable<Client> {
    return this.http.put<Client>(routes.get('updateClient')!, appointment);
  }
  deleteClient(appointment: Client): Observable<any> {
    return this.http.delete<any>(routes.get('deleteClient')!, {body: appointment});
  }

}
